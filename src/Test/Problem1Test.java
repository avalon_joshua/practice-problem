package Test;

import problem.Problem1;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Problem1Test implements ITest {
    private Problem1 problem;

    public Problem1Test(Problem1 problem) {
        this.problem = problem;
    }

    @Override
    public void run() {
        testShouldWork();
        testSumDouble();
        testStringTimes();
        testStringSplosion();
    }

    private void testShouldWork() {
        try {
            Utility.AssertPrint(false, problem.shouldWork(false, false));
            Utility.AssertPrint(true, problem.shouldWork(true, false));
            Utility.AssertPrint(false, problem.shouldWork(false, true));
            Utility.AssertPrint(false, problem.shouldWork(true, true));
        } catch (NotImplementedException e) {
            System.out.println("shouldWork is not implemented.");
        }
    }

    private void testSumDouble() {
        try {
            Utility.AssertPrint(3, problem.sumDouble(1, 2));
            Utility.AssertPrint(5, problem.sumDouble(3, 2));
            Utility.AssertPrint(8, problem.sumDouble(2, 2));
        } catch (NotImplementedException e) {
            System.out.println("sumDouble is not implemented.");
        }
    }

    private void testStringTimes() {
        try {
            Utility.AssertPrint("Hello World!Hello World!", problem.stringTimes("Hello World!", 2));
            Utility.AssertPrint("Hello World!", problem.stringTimes("Hello World!", 1));
            Utility.AssertPrint("LOL", problem.stringTimes("LOLLOLLOLLOLLOL", 5));
        } catch (NotImplementedException e) {
            System.out.println("stringTimes is not implemented.");
        }
    }

    private void testStringSplosion() {
        try {
            Utility.AssertPrint("aab", problem.stringSplosion("ab"));
            Utility.AssertPrint("aababc", problem.stringSplosion("abc"));
            Utility.AssertPrint("CCoCodCode", problem.stringSplosion("Code"));
        } catch (NotImplementedException e) {
            System.out.println("stringSplosion is not implemented.");
        }
    }
}
