package Test;

import problem.Problem2;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Problem2Test implements ITest {
    private Problem2 problem;

    public Problem2Test(Problem2 problem) {
        this.problem = problem;
    }

    @Override
    public void run() {
        testMaxArray();
        testMidThree();
        testLoneSum();
        testCountString();
        testCountYZ();
    }

    private void testMaxArray() {
        try {
            Utility.AssertPrint(new int[]{3, 3, 3}, problem.maxArray(new int[]{1, 2, 3}));
            Utility.AssertPrint(new int[]{11, 11, 11, 11}, problem.maxArray(new int[]{11, 5, 9, 4}));
        } catch (NotImplementedException e) {
            System.out.println("maxArray is not implemented.");
        }
    }

    private void testMidThree() {
        try {
            Utility.AssertPrint(new int[]{2, 3, 4}, problem.midThree(new int[]{1, 2, 3, 4, 5}));
            Utility.AssertPrint(new int[]{7, 5, 3}, problem.midThree(new int[]{8, 6, 7, 5, 3, 0, 9}));
            Utility.AssertPrint(new int[]{1, 2, 3}, problem.midThree(new int[]{1, 2, 3}));
        } catch (NotImplementedException e) {
            System.out.println("midThree is not implemented.");
        }
    }

    private void testLoneSum() {
        try {
            Utility.AssertPrint(6, problem.loneSum(1, 2, 3));
            Utility.AssertPrint(2, problem.loneSum(3, 2, 3));
            Utility.AssertPrint(0, problem.loneSum(3, 3, 3));
        } catch (NotImplementedException e) {
            System.out.println("midThree is not implemented.");
        }
    }

    private void testCountString() {
        try {
            Utility.AssertPrint(1, problem.countString("abc hi ho", "abc"));
            Utility.AssertPrint(2, problem.countString("xxAxxxBxxxAxx", "xxAxx"));
            Utility.AssertPrint(3, problem.countString("hiihihi", "hi"));
        } catch (NotImplementedException e) {
            System.out.println("countString is not implemented.");
        }
    }


    private void testCountYZ() {
        try {
            Utility.AssertPrint(2, problem.countYZ("fez day"));
            Utility.AssertPrint(3, problem.countYZ("day fyyyz rws zye kky"));
        } catch (NotImplementedException e) {
            System.out.println("countYZ is not implemented.");
        }
    }
}
