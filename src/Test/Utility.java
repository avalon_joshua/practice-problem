package Test;

import java.util.Arrays;

public class Utility {
    private static final String PRINT_OUT = "Expected: %s\tResult: %s\tCorrectness:%s";

    public static <T> void AssertPrint(T expected, T result) {
        System.out.println(String.format(PRINT_OUT, expected, result, expected.equals(result) ? "✓" : "×"));
    }

    public static void AssertPrint(boolean expected, boolean result) {
        System.out.println(String.format(PRINT_OUT, expected, result, expected == result ? "✓" : "×"));
    }

    public static void AssertPrint(int expected, int result) {
        System.out.println(String.format(PRINT_OUT, expected, result, expected == result ? "✓" : "×"));
    }

    public static void AssertPrint(int[] expected, int[] result) {
        System.out.println(String.format(PRINT_OUT, Arrays.toString(expected), Arrays.toString(result), Arrays.equals(expected, result) ? "✓" : "×"));
    }
}
