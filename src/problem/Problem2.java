package problem;

import Test.ITest;
import Test.Problem2Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Problem2 {
    public static void main(String[] args) {
        ITest test = new Problem2Test(new Problem2());
        test.run();
    }

    /*
        Problem 2.1
        Given an integer array. Find the largest integer in the array
        and return an array of the largest integer with same length.
        maxArray({1, 2, 3}) → {3, 3, 3}
     */
    public int[] maxArray(int[] intArray) {
        // TODO: Please read description above to implement and remove the exception below.
        throw new NotImplementedException();
    }

    /*
        Problem 2.2
        Given an array of ints of odd length,
        return a new array length 3 containing the elements from the middle of the array.
        The array length will be at least 3.
        midThree({1, 2, 3, 4, 5}) → {2, 3, 4}
        midThree({8, 6, 7, 5, 3, 0, 9}) → {7, 5, 3}
        midThree({1, 2, 3}) → {1, 2, 3}
     */
    public int[] midThree(int[] nums) {
        // TODO: Please read description above to implement and remove the exception below.
        throw new NotImplementedException();
    }

    /*
        Problem 2.3
        Given 3 int values, a b c, return their sum.
        However, if one of the values is the same as another of the values,
        It does not count towards the sum.
        loneSum(1, 2, 3) → 6
        loneSum(3, 2, 3) → 2
        loneSum(3, 3, 3) → 0
     */
    public int loneSum(int a, int b, int c) {
        // TODO: Please read description above to implement and remove the exception below.
        throw new NotImplementedException();
    }

    /*
        Problem 2.4
        Return the number of times that the substring appears anywhere in the given string str.
        countString("abc hi ho","abc") → 1
        countString("ABChi hi",hi) → 2
     */
    public int countString(String str, String substring) {
        // TODO: Please read description above to implement and remove the exception below.
        throw new NotImplementedException();
    }

    /*
        Problem 2.5
        Given a string, count the number of words ending in 'y' or 'z'
        countYZ("fez day") → 2
        countYZ("date fez") → 1
     */
    public int countYZ(String str) {
        // TODO: Please read description above to implement and remove the exception below.
        throw new NotImplementedException();
    }
}
