package problem;

import Test.ITest;
import Test.Problem1Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class Problem1 {
    public static void main(String[] args) {
        ITest test = new Problem1Test(new Problem1());
        test.run();
    }

    /*
        Problem 1.1
        The function should return should I work today.
        isWeekday represents that is it weekday toady.
        isVacation represents that am I in vacation (No need to work).
     */
    public boolean shouldWork(boolean isWeekday, boolean isVacation) {
        // TODO: Please read description above to implement and remove the exception below.
        throw new NotImplementedException();
    }

    /*
        Problem 1.2
        Given two int values, return their sum.
        Unless those int values are the same, then return double their sum.
     */
    public int sumDouble(int num1, int num2) {
        // TODO: Please read description above to implement and remove the exception below.
        throw new NotImplementedException();
    }

    /*
        Problem 1.3
        Given a string and a non-negative int n, return a larger string that is n copies of the original string.
        stringTimes("a", 2) → "aa"
     */
    public int stringTimes(String text, int n) {
        // TODO: Please read description above to implement and remove the exception below.
        throw new NotImplementedException();
    }

    /*
        Problem 1.4
        Given a non-empty string like "Code" return a string like "CCoCodCode".
        stringSplosion("Code") → "CCoCodCode"
        "C Co Cod Code"← break down like this.
     */
    public int stringSplosion(String text) {
        // TODO: Please read description above to implement and remove the exception below.
        throw new NotImplementedException();
    }
}
